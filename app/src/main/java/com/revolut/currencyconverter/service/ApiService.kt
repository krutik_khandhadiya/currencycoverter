package com.revolut.currencyconverter.service

import com.revolut.currencyconverter.domain.rate.model.RateList
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.QueryMap


interface ApiService {

    @GET("latest")
    fun getRateInfo(@QueryMap options: Map<String, String>): Flowable<RateList>
}