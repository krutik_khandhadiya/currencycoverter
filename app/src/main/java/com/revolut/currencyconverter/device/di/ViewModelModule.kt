package com.revolut.currencyconverter.device.di

import androidx.lifecycle.ViewModelProvider
import com.revolut.currencyconverter.presentation.rate.CurrencyRateViewModelImpl
import com.revolut.currencyconverter.root.BaseViewModel
import com.revolut.currencyconverter.root.ViewModelFactory
import com.revolut.currencyconverter.root.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyRateViewModelImpl::class)
    protected abstract fun bindCurrencyViewModelImpl(currencyRateViewModel: CurrencyRateViewModelImpl): BaseViewModel
}
