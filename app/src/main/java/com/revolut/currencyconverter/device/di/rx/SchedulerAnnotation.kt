package com.revolut.currencyconverter.device.di.rx

import javax.inject.Qualifier

@Qualifier
annotation class IoThreadScheduler

@Qualifier
annotation class MainThreadScheduler