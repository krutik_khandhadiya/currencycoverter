package com.revolut.currencyconverter.device.di

import android.app.Application
import com.revolut.currencyconverter.root.RevolutCurrencyApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        AppModule::class,
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        NetworkModule::class,
        RxModule::class,
        ViewModelModule::class
    )
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    fun inject(app: RevolutCurrencyApplication)

    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}
