package com.revolut.currencyconverter.device.di;


import com.revolut.currencyconverter.device.di.rx.IoThreadScheduler;
import com.revolut.currencyconverter.device.di.rx.IoThreadSchedulerProvider;
import com.revolut.currencyconverter.device.di.rx.MainThreadScheduler;
import com.revolut.currencyconverter.device.di.rx.MainThreadSchedulerProvider;
import com.revolut.currencyconverter.device.di.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public final class RxModule {

    private RxModule() {

    }
    @Provides
    static CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    @MainThreadScheduler
    static SchedulerProvider provideMainThreadSchedulerProvider() {
        return new MainThreadSchedulerProvider();
    }

    @Provides
    @Singleton
    @IoThreadScheduler
    static SchedulerProvider provideIoSchedulerProvider() {
        return new IoThreadSchedulerProvider();
    }
}