package com.revolut.currencyconverter.device.di.rx

import io.reactivex.Scheduler

interface SchedulerProvider {
    fun provideSchedulerProvider(): Scheduler
}