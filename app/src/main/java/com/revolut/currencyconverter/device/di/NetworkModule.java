package com.revolut.currencyconverter.device.di;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revolut.currencyconverter.BuildConfig;
import com.revolut.currencyconverter.device.di.model.ResponseWrapper;
import com.revolut.currencyconverter.service.ApiService;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public final class NetworkModule {


    private NetworkModule() {

    }

    private static Interceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT);
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }


    private static GsonConverterFactory provideGsonFactoryConvertor() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson userDeserializer = gsonBuilder.setLenient().registerTypeAdapter(ResponseWrapper.class, new UserResponseDeserializer()).create();
        return GsonConverterFactory.create(userDeserializer);
    }


    private static Retrofit provideRetrofit(
            String baseUrl,
            Converter.Factory converterFactory,
            CallAdapter.Factory callAdapterFactory) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG)
            client.addInterceptor(getHttpLoggingInterceptor());



        Retrofit.Builder builder = new Retrofit.Builder()
                .client(client.build())
                .addCallAdapterFactory(callAdapterFactory);
        return builder.baseUrl(baseUrl).addConverterFactory(converterFactory).build();
    }


    @Provides
    static RxJava2CallAdapterFactory provideRxJava2CallAdapterFactory() {
        return RxJava2CallAdapterFactory
                .createWithScheduler(Schedulers.io());
    }

    @Provides
    static ApiService provideApiService(
            RxJava2CallAdapterFactory rxJava2CallAdapterFactory) {
        return provideRetrofit(BuildConfig.BASE_URL,
                provideGsonFactoryConvertor(),
                rxJava2CallAdapterFactory).create(ApiService.class);
    }

}
