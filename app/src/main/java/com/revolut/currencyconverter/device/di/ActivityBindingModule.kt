package com.revolut.currencyconverter.device.di

import com.revolut.currencyconverter.presentation.rate.activity.ConverterActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    internal abstract fun bindConvertorActivity(): ConverterActivity
}