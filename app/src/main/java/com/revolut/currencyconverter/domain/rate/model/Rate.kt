package com.revolut.currencyconverter.domain.rate.model

data class Rate(val symbol: String, val rate: Float)