package com.revolut.currencyconverter.domain.rate.model

import java.math.BigDecimal

data class RateList(val base: String, val date: String, val rates: Map<String, BigDecimal>)