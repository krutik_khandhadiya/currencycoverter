package com.revolut.currencyconverter.domain.rate.usecase

import com.revolut.currencyconverter.data.source.RateDataRepository
import com.revolut.currencyconverter.device.di.rx.IoThreadScheduler
import com.revolut.currencyconverter.device.di.rx.MainThreadScheduler
import com.revolut.currencyconverter.device.di.rx.SchedulerProvider
import com.revolut.currencyconverter.domain.rate.model.RateList
import com.revolut.currencyconverter.domain.rate.model.RateRequest
import com.revolut.currencyconverter.presentation.ApiResponse
import com.revolut.currencyconverter.presentation.rate.model.CurrencyData
import com.revolut.currencyconverter.presentation.rate.model.CurrencyUIModel
import com.revolut.currencyconverter.presentation.util.format
import com.revolut.currencyconverter.root.UseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetCurrencyRateUseCase @Inject constructor(private val rateDataRepository: RateDataRepository, @IoThreadScheduler subscribeOnScheduler: SchedulerProvider, @MainThreadScheduler observeOnScheduler: SchedulerProvider) :
    UseCase<RateRequest, ApiResponse<CurrencyData>>() {

    override fun createObservable(request: RateRequest): Flowable<ApiResponse<CurrencyData>> {

        return rateDataRepository.getRateData(request).map { rateList: RateList -> rateList.rates }
            .map { rates ->
                rates.keys.map { value ->
                    CurrencyUIModel(value, rates[value]?.times(request.amount)?.format())
                }
            }.map { list ->
                buildApiResponse(list, request)
            }
    }

    fun buildApiResponse(
        list: List<CurrencyUIModel>,
        rateRequest: RateRequest
    ): ApiResponse<CurrencyData> {
        val ratelist: MutableList<CurrencyUIModel> = mutableListOf()
        ratelist.add(
            CurrencyUIModel(rateRequest.base, rateRequest.amount)
        )
        ratelist.addAll(list)
        return ApiResponse(
            STATUS_CODE,
            STATUS_MESSAGE, CurrencyData(ratelist)
        )
    }

    companion object {
        const val STATUS_CODE = 200
        const val STATUS_MESSAGE = "Got respone"
    }

}






