package com.revolut.currencyconverter.domain.rate.model

import java.math.BigDecimal

data class RateRequest(val base: String,val amount:BigDecimal) {
    fun getQueryMap(): Map<String, String> {
        val map = HashMap<String, String>()
        map.put("base", base)
        return map
    }
}