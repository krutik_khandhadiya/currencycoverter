package com.revolut.currencyconverter.presentation

import androidx.lifecycle.MutableLiveData
import com.revolut.currencyconverter.domain.rate.model.RateRequest
import com.revolut.currencyconverter.presentation.rate.model.CurrencyData
import com.revolut.currencyconverter.presentation.util.Resource
import com.revolut.currencyconverter.root.BaseViewModel


interface CurrencyRateViewModel : BaseViewModel {

    fun getCurrencyData(rateRequest: RateRequest,viewChanged: Boolean): MutableLiveData<Resource<CurrencyData>>
}
