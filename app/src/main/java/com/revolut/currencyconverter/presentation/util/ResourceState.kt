package com.revolut.currencyconverter.presentation.util

enum class ResourceState {
    LOADING, SUCCESS, EMPTY, ERROR
}