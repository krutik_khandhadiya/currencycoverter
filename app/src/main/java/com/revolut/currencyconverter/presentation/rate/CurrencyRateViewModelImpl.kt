package com.revolut.currencyconverter.presentation.rate

import androidx.lifecycle.MutableLiveData
import com.revolut.currencyconverter.domain.rate.model.RateRequest
import com.revolut.currencyconverter.domain.rate.usecase.GetCurrencyRateUseCase
import com.revolut.currencyconverter.presentation.ApiResponse
import com.revolut.currencyconverter.presentation.CurrencyRateViewModel
import com.revolut.currencyconverter.presentation.rate.model.CurrencyData
import com.revolut.currencyconverter.presentation.util.Resource
import com.revolut.currencyconverter.presentation.util.error
import com.revolut.currencyconverter.presentation.util.success
import com.revolut.currencyconverter.root.BaseViewModelImpl
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class CurrencyRateViewModelImpl @Inject constructor(
    private val getCurrencyRateUseCase: GetCurrencyRateUseCase,
    compositeDisposable: CompositeDisposable
) :
    BaseViewModelImpl(compositeDisposable), CurrencyRateViewModel {
    private val currencyRateLiveData = MutableLiveData<Resource<CurrencyData>>()

    private var currentBase: String = ""


    override fun getCurrencyData(rateRequest: RateRequest, viewChanged: Boolean):
            MutableLiveData<Resource<CurrencyData>> {

        if (viewChanged)
            compositeDisposable.clear()

        manageActionDisposables(
            getCurrencyRateUseCase.executeWithInterval(
                rateRequest,
                1L
            ).repeat()
                .subscribe({ response: ApiResponse<CurrencyData>? ->
                    response?.let {
                        currentBase = rateRequest.base
                        response.data?.let {
                            currencyRateLiveData.success(it)
                        } ?: run {
                            currencyRateLiveData.error(response.statusCode)
                        }

                    } ?: run {
                        currencyRateLiveData.error(-1)
                    }

                }, { t: Throwable? ->
                    currencyRateLiveData.error(-1)
                    t?.printStackTrace()
                }

                ))
        return currencyRateLiveData

    }


}

