package com.revolut.currencyconverter.presentation.rate.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Parcelize
data class CurrencyUIModel(
    val symbolUrl: String = "",
    val rate: BigDecimal?
) : Parcelable