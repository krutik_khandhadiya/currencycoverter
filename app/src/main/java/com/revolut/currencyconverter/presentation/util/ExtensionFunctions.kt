package com.revolut.currencyconverter.presentation.util

import androidx.lifecycle.MutableLiveData
import java.math.BigDecimal


fun <T> MutableLiveData<Resource<T>>.loading() {
    this.postValue(Resource.loading())
}

fun <T> MutableLiveData<Resource<T>>.success(data: T?) {
    this.postValue(Resource.success(data))
}

fun <T> MutableLiveData<Resource<T>>.emptyData(message: Int? = null) {
    this.postValue(Resource.error(message))
}

fun <T> MutableLiveData<Resource<T>>.error(statuscode: Int?) {
    this.postValue(Resource.error(statuscode))
}

fun BigDecimal.format() : BigDecimal =  setScale(2, BigDecimal.ROUND_DOWN);