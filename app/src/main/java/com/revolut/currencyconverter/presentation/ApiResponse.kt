package com.revolut.currencyconverter.presentation

data class ApiResponse<out T>(val statusCode: Int?, val message: String?, val data: T?)