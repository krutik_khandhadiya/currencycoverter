package com.revolut.currencyconverter.presentation.rate.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CurrencyData(val currencyUIModel: MutableList<CurrencyUIModel>) : Parcelable