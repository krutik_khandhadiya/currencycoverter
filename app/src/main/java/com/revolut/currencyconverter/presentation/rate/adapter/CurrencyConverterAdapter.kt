package com.revolut.currencyconverter.presentation.rate.adapter


import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.revolut.currencyconverter.R
import com.revolut.currencyconverter.presentation.rate.listener.OnAmountChangedListener
import com.revolut.currencyconverter.presentation.rate.model.CurrencyUIModel
import com.revolut.currencyconverter.presentation.util.getCurrencyFlagResId
import com.revolut.currencyconverter.presentation.util.getCurrencyNameResId
import kotlinx.android.synthetic.main.item_currency_convert.view.*
import java.util.*
import kotlin.collections.ArrayList


class CurrencyConverterAdapter(private val onAmountChangedListener: OnAmountChangedListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val symbolPosition = ArrayList<String>()
    private val symbolRate = HashMap<String, CurrencyUIModel>()


    companion object {
        const val PAY_LOAD = 100
    }

    fun updateRates(rates: List<CurrencyUIModel>) {
        if (symbolPosition.isEmpty()) {
            symbolPosition.addAll(rates.map { it.symbolUrl })
        }

        for (rate in rates) {
            symbolRate[rate.symbolUrl] = rate
        }

        notifyItemRangeChanged(0, symbolPosition.size - 1, PAY_LOAD)
    }

    private fun rateAtPosition(pos: Int): CurrencyUIModel? {
        return symbolRate[symbolPosition[pos]]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RateViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_currency_convert, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return symbolPosition.size
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: List<Any>
    ) {
        if (!payloads.isEmpty()) {
            (holder as RateViewHolder).bind(rateAtPosition(position))
        } else {
            super.onBindViewHolder(holder, position, payloads)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as RateViewHolder).bind(rateAtPosition(position))
    }


    inner class RateViewHolder(itemView: View) : RecyclerView.ViewHolder(
        itemView
    ) {

        var icCurrencyFlag: ImageView = itemView.icCurrencyFlag
        var lblCurrencySymbol: TextView = itemView.lblCurrencySymbol
        var lblCurrencyName: TextView = itemView.lblCurrencyName
        var txtCurrencyAmount: EditText = itemView.txtCurrencyAmount
        var symbol: String = ""


        fun bind(rate: CurrencyUIModel?) {
            rate?.apply {
                if (symbol != symbolUrl) {
                    initView(this)
                    symbol = symbolUrl
                }

                if (!txtCurrencyAmount.isFocused) {
                    txtCurrencyAmount.setText((this.rate?.toString()))
                }
            }
        }

        private fun initView(rate: CurrencyUIModel) {
            val symbol = rate.symbolUrl.toLowerCase(Locale.getDefault())
            val nameId = getCurrencyNameResId(itemView.context, symbol)
            val flagId = getCurrencyFlagResId(itemView.context, symbol)

            lblCurrencySymbol.text = rate.symbolUrl
            lblCurrencyName.text = itemView.context.getString(nameId)
            icCurrencyFlag.setImageResource(flagId)

            txtCurrencyAmount.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->

                if (!hasFocus) {
                    return@OnFocusChangeListener
                }

                layoutPosition.takeIf { it > 0 }?.also { currentPosition ->
                    symbolPosition.removeAt(currentPosition).also {

                        symbolPosition.add(0, it)
                    }
                    notifyItemMoved(currentPosition, 0)
                }
            }

            txtCurrencyAmount.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.let {
                        if (txtCurrencyAmount.isFocused && s.isNotEmpty()) {
                            onAmountChangedListener.onAmountChanged(
                                symbol,
                                s.toString().toBigDecimal()
                            )
                        }
                    }
                }
            })
        }
    }
}