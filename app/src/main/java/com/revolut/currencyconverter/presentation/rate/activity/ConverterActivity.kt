package com.revolut.currencyconverter.presentation.rate.activity

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.revolut.currencyconverter.R
import com.revolut.currencyconverter.domain.rate.model.RateRequest
import com.revolut.currencyconverter.presentation.CurrencyRateViewModel
import com.revolut.currencyconverter.presentation.rate.CurrencyRateViewModelImpl
import com.revolut.currencyconverter.presentation.rate.adapter.CurrencyConverterAdapter
import com.revolut.currencyconverter.presentation.rate.listener.OnAmountChangedListener
import com.revolut.currencyconverter.presentation.rate.model.CurrencyData
import com.revolut.currencyconverter.presentation.rate.model.CurrencyUIModel
import com.revolut.currencyconverter.presentation.util.Resource
import com.revolut.currencyconverter.presentation.util.ResourceState
import com.revolut.currencyconverter.root.BaseActivity
import kotlinx.android.synthetic.main.activity_converter.*
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject


class ConverterActivity : BaseActivity<CurrencyRateViewModel>(), OnAmountChangedListener {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var rateList = mutableListOf<CurrencyUIModel>()

    override fun getAssociatedViewModel(): CurrencyRateViewModel {
        return ViewModelProviders.of(this, viewModelFactory)
            .get(CurrencyRateViewModelImpl::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_converter)
        txtEmpty.visibility = View.GONE
        progressLoad.visibility = View.VISIBLE
        rvRates.layoutManager = LinearLayoutManager(this)


        rvRates.adapter = CurrencyConverterAdapter(this)

        window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )

        getData(RateRequest(BASE_CURRENCY, BASE_CURRENCY_VALUE.toBigDecimal()), false)
    }

    val rateObeserver = Observer<Resource<CurrencyData>> { data ->
        window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        progressLoad.visibility = View.GONE
        txtEmpty.visibility = View.GONE
        if (data.status == ResourceState.SUCCESS) {
            data.data?.let {
                rateList.addAll(it.currencyUIModel)
                (rvRates.adapter as CurrencyConverterAdapter).updateRates(rateList)
            } ?: run {
                txtEmpty.apply {
                    text = getString(R.string.message_no_data)
                    visibility = View.VISIBLE
                }
                rvRates.visibility = View.GONE
            }
        } else if (data.status == ResourceState.ERROR) {
            txtEmpty.apply {
                text = getString(R.string.message_no_internet)
                visibility = View.VISIBLE
            }
            rvRates.visibility = View.GONE
        }

    }

    override fun onAmountChanged(symbol: String, amount: BigDecimal) {
        getData(RateRequest(symbol.toUpperCase(Locale.ENGLISH), amount), true)
    }

    fun getData(rateRequest: RateRequest, viewChanged: Boolean) {

        val rateLiveData =
            viewModel.getCurrencyData(rateRequest, viewChanged)
        rateLiveData.observe(this, rateObeserver)
    }

    companion object {
        const val BASE_CURRENCY = "EUR"
        const val BASE_CURRENCY_VALUE = 100
    }

}