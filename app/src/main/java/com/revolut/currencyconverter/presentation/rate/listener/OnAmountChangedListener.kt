package com.revolut.currencyconverter.presentation.rate.listener

import java.math.BigDecimal

interface OnAmountChangedListener {


    fun onAmountChanged(symbol: String, amount: BigDecimal)
}