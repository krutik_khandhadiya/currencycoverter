package com.revolut.currencyconverter.root

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity


abstract class BaseActivity<T : BaseViewModel> : BaseView<T>, DaggerAppCompatActivity() {

    protected lateinit var viewModel: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getAssociatedViewModel()
    }
}