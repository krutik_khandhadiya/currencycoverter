package com.revolut.currencyconverter.root

import android.app.Activity
import android.app.Application
import com.revolut.currencyconverter.device.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class RevolutCurrencyApplication : Application(), HasActivityInjector {

    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
        @Inject set

    override fun onCreate() {
        super.onCreate()
        // Initialize Dagger
        DaggerAppComponent
            .builder()
            .application(this)
            .build()
            .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }
}
