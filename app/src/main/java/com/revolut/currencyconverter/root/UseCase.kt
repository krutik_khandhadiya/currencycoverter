package com.revolut.currencyconverter.root


import com.revolut.currencyconverter.device.di.rx.IoThreadSchedulerProvider
import com.revolut.currencyconverter.device.di.rx.MainThreadSchedulerProvider
import com.revolut.currencyconverter.device.di.rx.SchedulerProvider
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

abstract class UseCase<REQUEST, RESPONSE> @JvmOverloads constructor(
    private val subscribeOnScheduler: SchedulerProvider = IoThreadSchedulerProvider(),
    private val observeOnScheduler: SchedulerProvider = MainThreadSchedulerProvider()
) {

    fun execute(request: REQUEST): Flowable<RESPONSE> {
        return createObservable(request)
            .subscribeOn(subscribeOnScheduler.provideSchedulerProvider())
            .observeOn(observeOnScheduler.provideSchedulerProvider())
    }

    fun executeWithInterval(request: REQUEST, interval: Long): Flowable<RESPONSE> {
        return createObservable(request).delay(interval, TimeUnit.SECONDS)
            .subscribeOn(subscribeOnScheduler.provideSchedulerProvider())
            .observeOn(observeOnScheduler.provideSchedulerProvider())
    }

    protected abstract fun createObservable(request: REQUEST): Flowable<RESPONSE>
}
