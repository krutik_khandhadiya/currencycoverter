package com.revolut.currencyconverter.data.source

import com.revolut.currencyconverter.domain.rate.model.RateList
import com.revolut.currencyconverter.domain.rate.model.RateRequest
import com.revolut.currencyconverter.service.ApiService
import io.reactivex.Flowable
import javax.inject.Inject

class RateDataRepository
@Inject constructor(private val apiServices: ApiService) {

    fun getRateData(request: RateRequest): Flowable<RateList> {
        return apiServices.getRateInfo(request.getQueryMap())
    }
}