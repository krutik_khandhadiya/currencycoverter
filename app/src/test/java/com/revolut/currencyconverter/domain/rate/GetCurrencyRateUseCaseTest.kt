package com.revolut.currencyconverter.domain.rate


import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.whenever
import com.revolut.currencyconverter.data.source.RateDataRepository
import com.revolut.currencyconverter.domain.rate.model.RateList
import com.revolut.currencyconverter.domain.rate.model.RateRequest
import com.revolut.currencyconverter.domain.rate.usecase.GetCurrencyRateUseCase
import com.revolut.currencyconverter.presentation.ApiResponse
import com.revolut.currencyconverter.presentation.rate.model.CurrencyData
import com.revolut.currencyconverter.util.RxImmediateSchedulerRule
import com.revolut.currencyconverter.util.TestSchedulerProvider
import io.reactivex.Flowable
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetCurrencyRateUseCaseTest {
    private lateinit var getRateDataUseCase: GetCurrencyRateUseCase

    @Mock
    lateinit var rateDataRepository: RateDataRepository


    lateinit var rateList: RateList

    val rateRequest = RateRequest("EU", 10.00.toBigDecimal())


    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Before
    fun setUp() {

        getRateDataUseCase =
            GetCurrencyRateUseCase(
                rateDataRepository,
                TestSchedulerProvider(),
                TestSchedulerProvider()
            )

        val rateData = this.javaClass.classLoader.getResourceAsStream("CurrencyData.json")
        val rateDataString = rateData.bufferedReader().use { it.readText() }

        val gson = Gson()

        rateList = gson.fromJson(rateDataString, RateList::class.java)
    }


    @Test
    fun testExecuteSizeCount() {
        whenever(rateDataRepository.getRateData(rateRequest))
            .thenReturn(Flowable.just(rateList))

        val testObserver = getRateDataUseCase.execute(rateRequest).test()

        testObserver.assertNoErrors()
        testObserver.assertValue { output: ApiResponse<CurrencyData> -> output.data?.currencyUIModel?.size == 33 }
    }

    @Test
    fun testExecuteConversionCheck() {
        whenever(rateDataRepository.getRateData(rateRequest))
            .thenReturn(Flowable.just(rateList))

        val testObserver = getRateDataUseCase.execute(rateRequest).test()

        testObserver.assertNoErrors()
        testObserver.assertValue { output: ApiResponse<CurrencyData> ->
            output.data?.currencyUIModel?.get(1)?.rate?.equals(8.24.toBigDecimal())!!
        }
    }
}