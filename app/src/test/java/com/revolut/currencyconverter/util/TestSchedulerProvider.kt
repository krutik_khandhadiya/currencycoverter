package com.revolut.currencyconverter.util



import com.revolut.currencyconverter.device.di.rx.SchedulerProvider
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class TestSchedulerProvider() : SchedulerProvider {
    override fun provideSchedulerProvider(): Scheduler {
        return Schedulers.trampoline()
    }
}